
public class BankAccount {
	private double balance;
	
	public void deposit(String amount){
		double d = Double.parseDouble(amount);
		this.balance += d;
	}
	
	public void withdraw(String amount){
		double w = Double.parseDouble(amount);
		this.balance -= w;
	}
	
	public String getBalance(){
		return this.balance+"";
	}
}
