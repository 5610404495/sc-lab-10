public class Controller {
	private BankAccount b;
	private int error;
	
	public Controller(){
		b = new BankAccount(); 
	}
	
	public void deposit(String p){
		b.deposit(p);
	}
	
	public void withdraw(String p){
		double amout = Double.parseDouble(p);
		double balance = Double.parseDouble(b.getBalance());

		if (balance < amout){
			error = 1;
		}
		else{
			error = 0;
			b.withdraw(p);
		}
	}
	
	public String getBalance(){
		return b.getBalance();
	}
	
	public int getValue(){
		return error;
	}
} 
