import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;

public class Gui extends JFrame{
	JFrame frame;
	
	private JPanel p1,p2;
	private JComboBox combobox;
	private JCheckBox redbox,bluebox,greenbox;
	private JRadioButton redbut,greenbut,bluebut;
	private JButton red,green,blue,comB;
	private ActionListener listener;
	private ButtonGroup g1,g2;
	private JMenuBar menu;
	private JMenu colorList;
	private JMenuItem redi,greeni,bluei;

	public Gui(){
		p1 = new JPanel();	
		p1.setSize(500, 200);
		p2 = new JPanel();
		p2.setSize(500, 20);

		String [] color = {"red","green","blue"};
		combobox = new JComboBox(color);
		
		createButton();
		add(p1);
		add(red);
		add(green);
		add(blue);
		p1.add(redbut);
		p1.add(greenbut);
		p1.add(bluebut);
		p1.add(redbox);
		p1.add(greenbox);
		p1.add(bluebox);
		p1.add(combobox);
		p1.add(comB);
		menu.add(colorList);
		p2.add(menu);
		p1.add(p2);
	}
	private void red(java.awt.event.WindowEvent evt) {
		   this.getContentPane().setBackground(Color.red);
		   p1.setBackground(Color.red);
		}
	private void green(java.awt.event.WindowEvent evt) {
		   this.getContentPane().setBackground(Color.green);
		   p1.setBackground(Color.green);
		}
	private void blue(java.awt.event.WindowEvent evt) {
		   this.getContentPane().setBackground(Color.blue);
		   p1.setBackground(Color.blue);
		}
	private void yellow(java.awt.event.WindowEvent evt) {
		   this.getContentPane().setBackground(new Color(255,255,0));
		   p1.setBackground(new Color(255,255,0));
		}
	private void purple(java.awt.event.WindowEvent evt) {
		   this.getContentPane().setBackground(new Color(255,0,255));
		   p1.setBackground(new Color(255,0,255));
		}
	private void sky(java.awt.event.WindowEvent evt) {
		   this.getContentPane().setBackground(new Color(0,255,255));
		   p1.setBackground(new Color(0,255,255));
		}
	private void white(java.awt.event.WindowEvent evt) {
		   this.getContentPane().setBackground(Color.WHITE);
		   p1.setBackground(Color.WHITE);
		}
		
		public void createButton(){
			
			
			red = new JButton("red");
			red.addActionListener(listener);
			red.setSize(100,50);
			red.setLocation(80, 300);
			red.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent evt) {
					combobox.setSelectedIndex(0);
					redbut.setSelected(true);
					red(null);}}
				
				);    
			
			blue = new JButton("blue");
			blue.addActionListener(listener);
			blue.setSize(100,50);
			blue.setLocation(200, 300);
			blue.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent evt) {
					combobox.setSelectedIndex(2);
					bluebut.setSelected(true);
					blue(null);}}
				
				);    
			
			green = new JButton("green");
			green.setSize(100,50);
			green.setLocation(320, 300);
			green.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent evt) {
					combobox.setSelectedIndex(1);
					greenbut.setSelected(true);
					green(null);}}				
				); 
			
			comB = new JButton("Select");
			comB.setSize(100,50);
			comB.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					String c = combobox.getSelectedItem().toString();
					if (c == "red"){
						redbut.setSelected(true);
						red(null);
					}
					if (c == "green"){
						greenbut.setSelected(true);
						green(null);
					}
					if (c == "blue"){
						bluebut.setSelected(true);
						blue(null);
					}
					}}				
				); 
			
			redbut = new JRadioButton("red");
			redbut.addActionListener(listener);
			redbut.addActionListener(new ActionListener() {
			
				public void actionPerformed(ActionEvent evt) {
				
				red(null);}}
				
				);
			greenbut = new JRadioButton("green");
			greenbut.addActionListener(listener);
			greenbut.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent evt) {
				
				green(null);}}
				
				);
			bluebut = new JRadioButton("blue");
			bluebut.addActionListener(listener);
			bluebut.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent evt) {
				
				blue(null);}}
				
				);
			g1 = new ButtonGroup();
			g1.add(redbut);
			g1.add(greenbut);
			g1.add(bluebut);
			
			
			
			redbox = new JCheckBox("red");
			redbox.addActionListener(listener);
			redbox.setLocation(80, 120);
			redbox.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					boolean gr = greenbox.isSelected();
					boolean bl = bluebox.isSelected();
					if(gr == true){
						combobox.setSelectedIndex(0);
						redbut.setSelected(true);
						yellow(null);
					}
					if(bl == true){
						combobox.setSelectedIndex(0);
						redbut.setSelected(true);
						purple(null);
					}
					if(gr==false & bl==false){
						red(null);}
					if(gr==true & bl==true){
						white(null);
					}
				}}
					
				);    
			greenbox = new JCheckBox("green");
			greenbox.setLocation(320, 120);
			greenbox.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					boolean re = redbox.isSelected();
					boolean bl = bluebox.isSelected();
					if(re == true){
						combobox.setSelectedIndex(0);
						greenbut.setSelected(true);
						yellow(null);
					}
					if(bl == true){
						combobox.setSelectedIndex(0);
						redbut.setSelected(true);
						sky(null);
					}
					if(re==false & bl==false){
						green(null);}
					if(re==true & bl==true){
						white(null);
					}
				}}
					);  
			
			bluebox = new JCheckBox("blue");
			bluebox.addActionListener(listener);
			bluebox.setLocation(200, 120);
			bluebox.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					boolean re = redbox.isSelected();
					boolean gr = greenbox.isSelected();
					if(re == true){
						combobox.setSelectedIndex(0);
						bluebut.setSelected(true);
						purple(null);
					}
					if(gr == true){
						combobox.setSelectedIndex(0);
						bluebut.setSelected(true);
						sky(null);
					}
					if(re==false & gr==false){
						green(null);}
					if(re==true & gr==true){
						white(null);
					}
				}}
					);
			menu = new JMenuBar();
			
			colorList = new JMenu("Color");
			redi = new JMenuItem("Red");
			greeni = new JMenuItem("Green");
			bluei = new JMenuItem("Blue");
		
			colorList.add(redi);
			colorList.add(greeni);
			colorList.add(bluei);	
			
			redi.addActionListener(new ActionListener() {			
				@Override
				public void actionPerformed(ActionEvent e) {
					red(null);
				}
			});
			
			greeni.addActionListener(new ActionListener() {			
				@Override
				public void actionPerformed(ActionEvent e) {
					green(null);
				}
			});
			
			bluei.addActionListener(new ActionListener() {			
				@Override
				public void actionPerformed(ActionEvent e) {
					blue(null);
				}
			});
			
			
		}
}
		